'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', [
  'ngRoute',
  'myApp.filters',
  'myApp.services',
  'myApp.directives',
  'myApp.controllers'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/', {templateUrl: 'partials/frontpage.html', controller: 'FrontPageCtrl'});
  $routeProvider.when('/post/:postID', {templateUrl: 'partials/post.html', controller: 'PostCtrl'});
  $routeProvider.when('/submit', {templateUrl: 'partials/submit.html', controller: 'SubmitCtrl'});
  //$routeProvider.when('/login', {templateUrl: 'partials/login.html', controller: 'LoginCtrl'});  
  $routeProvider.otherwise({redirectTo: '/'});
}]);
