'use strict';

/* Controllers */

angular.module('myApp.controllers', [])
  .controller('FrontPageCtrl', ['$scope', '$http','tokenFactory',function($scope, $http, tokenFactory) {


  		$http.get('../post').success(function(data,status,headers,config) {
  			$scope.foo = data;
  			$scope.posts = data;
  		}).
	  	error(function(data,status,headers,config) {

  		});


  	$scope.upvote = function(submission) {
  		console.log(submission._id);
  		var url = '../api/post/' + submission._id + '/vote?token=' + tokenFactory.token;
  		var data = {'vote': 1};
  		$http.put(url, data).success(function(data, status) {
  			submission.upvotes += 1;
  			console.log(data);
  		});

  	}

  	$scope.downvote = function(submission) {
  		console.log(submission._id);
  		var url = '../api/post/' + submission._id + '/vote?token=' + tokenFactory.token;
  		var data = {'vote': -1};
  		$http.put(url, data).success(function(data, status) {
  			submission.downvotes += 1;
  			console.log(data);
  		});

  	}
  }])


  .controller('SubmitCtrl', ['$scope', '$http','$location', 'tokenFactory',function($scope, $http, $location, tokenFactory) {
  	$scope.submitPost = function(submission) {
  		//var url = 'http://localhost:3000/api/post?token=' + tokenFactory.token;
  		var url = '../api/post?token=' + tokenFactory.token;

  		var data = {'title':submission.title, 'comment':submission.comment};
  		$http.post(url,data).success(function(data, status) {
  			console.log(data);
  			$location.path('/post/' + data._id)
  		});
  	}
  }])


  .controller('PostCtrl', ['$scope', '$http','$routeParams',function($scope, $http, $routeParams) {
  	$http.get('../post/' + $routeParams.postID).success(function(data,status,headers,config) {
  		//$scope.foo = data;
  		$scope.post = data;
  	}).
	 	error(function(data,status,headers,config) {

  	});

  	//$scope.foo = $routeParams.postID;
  	$scope.post = {};
  }])


  .controller('LoginCtrl', ['$scope', '$http', 'tokenFactory', function($scope,$http,tokenFactory) {
 		$scope.login_status = "";
 		$scope.logged_in = false;

 		$scope.show_registration = false;
 		$scope.show_login = false;

 		$scope.login = function(credentials) {
 			$http.post('../api/login', {'username':credentials.username, 'password':credentials.password}).success(function (data, status) {
 				tokenFactory.token = data.token;
 				tokenFactory.username = credentials.username;
 				$scope.login_status = "login succeeded";
 				$scope.logged_in = true;
 				$scope.show_login = false;
 				console.log(tokenFactory);
 			})
 			.error(function(data,status) {
 				$scope.login_status = "login failed";
 			});
 		}

 		$scope.toggleRegistration = function() {
 			if($scope.show_registration)
 				$scope.show_registration = false;
 			else {
 				$scope.show_registration = true;
 				$scope.show_login = false;
 			}
 		}

 		$scope.toggleLogin = function() {
 			if($scope.show_login)
 				$scope.show_login = false;
 			else {
 				$scope.show_login = true;
 				$scope.show_registration = false;
 			}
 		}
 		$scope.register = function(credentials) {
 			$http.post('../api/register', {'username':credentials.username, 'password':credentials.password, 'email':credentials.email}).success(function(data,status) {
 				tokenFactory.token = data.token;
 				tokenFactory.username = credentials.username;
 				$scope.login_status = "login succeeded";
 				$scope.logged_in = true;
 				$scope.show_registration = false;
 			})
 			.error(function(data,status) {
 				$scope.login_status = "login failed";
 			});
 		}
  }]);